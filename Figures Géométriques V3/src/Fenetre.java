import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseListener;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import Controleur.*;
import Modele.*;
import Vue.*;

public class Fenetre extends JFrame{
	
	
	VueDessin dessin;
	DessinModele model;
	
	
	public Fenetre(String s, int l, int h) throws IOException{
		
		
		this.dessin = new VueDessin();
		this.dessin.setPreferredSize(new Dimension(l,70));
		
		this.model = new DessinModele();
		this.model.addObserver(dessin);
		
		PanneauChoix boutons = new PanneauChoix(model);
		
		
		this.setTitle(s);
		this.setPreferredSize(new Dimension(l,h));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		
		this.add(boutons,BorderLayout.NORTH);
		
		this.add(this.dessin, BorderLayout.CENTER);
	
		
		
		
		
		this.pack();
		this.requestFocusInWindow();
		this.setVisible(true);
		
		FabricantFigures ff = new FabricantFigures(model);
		ManipulateurFormes mf = new ManipulateurFormes(model);
		dessin.addMouseListener(ff);
		dessin.addMouseListener(mf);
		dessin.addMouseMotionListener(mf);
		
		
		
		
	}

	public static void main(String[] args) throws IOException {
		
		Fenetre f = new Fenetre("Formes géometriques", 1400,900);
		
	}

}
