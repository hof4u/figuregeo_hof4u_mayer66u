package Vue;

import java.awt.*;
import java.util.*;

import javax.swing.*;

import Modele.*;

public class VueDessin extends JPanel implements Observer{
	
	private DessinModele salade;
	
	public VueDessin(){
		salade = new DessinModele();
		this.setPreferredSize(new Dimension(500,500));
		this.setBackground(Color.WHITE);
	}

	@Override
	public void update(Observable o, Object arg) {
		salade = (DessinModele)o;
		repaint();
		
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		for(int i=0;i<salade.getClics().size();i++){
			g.setColor(Color.BLACK);
			g.drawLine(this.salade.getClics().get(i).rendreX()-5, this.salade.getClics().get(i).rendreY(), this.salade.getClics().get(i).rendreX()+5, this.salade.getClics().get(i).rendreY());
			g.drawLine(this.salade.getClics().get(i).rendreX(), this.salade.getClics().get(i).rendreY()-5, this.salade.getClics().get(i).rendreX(), this.salade.getClics().get(i).rendreY()+5);
		}
		for(int i=0;i<salade.getLfg().size();i++){
			salade.getLfg().get(i).affiche(g);
			
		}
		
		
	}

}
