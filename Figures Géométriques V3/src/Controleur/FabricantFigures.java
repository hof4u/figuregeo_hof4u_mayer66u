package Controleur;

import java.awt.Color;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

import Modele.*;

public class FabricantFigures implements MouseListener,MouseMotionListener{

	private DessinModele model;
	private int count=0;
	private Point pq[] ;
	private Point pr[] ;
	private Point pt[] ;
	private Color c=Color.BLACK;
	int lastX,lastY;
	
	@Override
	public void mouseClicked(MouseEvent e) {
		
			
		
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public void mouseDragged(MouseEvent e) {
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public FabricantFigures(DessinModele odm){
		this.model=odm;
		pq = new Point[4];
		pr = new Point[2];
		pt = new Point[3];
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
		if(this.model.getNbClic()==4){
			int x=e.getX();
			int y=e.getY();
			this.model.ajoutePt(x, y);
			pq[count]=new Point(x,y);
			this.count++;
			if(count==4){
				FigureColoree fc;
				fc = this.model.getFigureEnCours();
				fc.modifierPoints(pq);
				fc.deSelectionne();
				c=fc.getColor();
				this.model.ajoute(fc);
				this.model.construit(new Quadrilatere(),c);
				this.count=0;
				
			}
		}
		
		if(this.model.getNbClic()==3){
			int x=e.getX();
			int y=e.getY();
			this.model.ajoutePt(x, y);
			pt[count]=new Point(x,y);
			this.count++;
			if(count==3){
				FigureColoree fc;
				fc = this.model.getFigureEnCours();
				fc.modifierPoints(pt);
				fc.deSelectionne();
				c=fc.getColor();
				this.model.ajoute(fc);
				this.model.construit(new Triangle(),c);
				this.count=0;
				
			}
		}
		
		if(this.model.getNbClic()==2){
			int x=e.getX();
			int y=e.getY();
			this.model.ajoutePt(x, y);
			pr[count]=new Point(x,y);
			this.count++;
			if(count==2){
				FigureColoree fc;
				fc = this.model.getFigureEnCours();
				fc.modifierPoints(pr);
				fc.deSelectionne();
				c=fc.getColor();
				this.model.ajoute(fc);
				this.model.construit(new Rectangle(),c);
				this.count=0;
			}
		}
		
		
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}



}
