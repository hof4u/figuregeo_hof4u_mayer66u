package Controleur;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import Modele.*;
import Modele.Rectangle;

public class PanneauChoix extends JPanel{
	
	private FigureColoree fc;
	private DessinModele dessin;
	private JComboBox jcb;
	
	public PanneauChoix(DessinModele des){
		this.setLayout(new BorderLayout());
		this.dessin=des;
		JRadioButton nf = new JRadioButton("Nouvelle Figure");
		JRadioButton tm = new JRadioButton("Trace � main lev�e");
		JRadioButton man = new JRadioButton("Manipulation");
		jcb = new JComboBox(new String[] { "Noir", "Bleu", "Jaune", "Vert","Gris","Violet","Rose","Rouge" });
		JComboBox formes = new JComboBox(new String[] { "Quadrilat�re" , "Rectangle" , "Triangle"});
		
		nf.setSelected(true);
		nf.addActionListener(new ActionListener(){

			
			
			@Override
			public void actionPerformed(ActionEvent e) {
				formes.setEnabled(true);
				jcb.setEnabled(true);
				for(int i=0; i<dessin.getLfg().size();i++){
					dessin.getLfg().get(i).deSelectionne();
				}
				fc=creeFigure(formes.getSelectedIndex());
				
				
						
				
			}
		});
		
		tm.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				formes.setEnabled(false);
				jcb.setEnabled(true);
				for(int i=0; i<dessin.getLfg().size();i++){
					dessin.getLfg().get(i).deSelectionne();
				}
				dessin.setNbClic(1);
				
			}
		});
		
		man.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e) {
				formes.setEnabled(false);
				jcb.setEnabled(false);
				for(int i=0; i<dessin.getLfg().size();i++){
					dessin.getLfg().get(i).deSelectionne();
				}
				dessin.setNbClic(0);						
				
			}
		});
		
		ButtonGroup bg = new ButtonGroup();
		bg.add(nf);
		bg.add(tm);
		bg.add(man);
		JPanel typeDessin = new JPanel();
		
		typeDessin.add(nf);
		typeDessin.add(tm);
		typeDessin.add(man);
		
		this.add(typeDessin, BorderLayout.NORTH);
		
		
		formes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fc=creeFigure(formes.getSelectedIndex());
				dessin.construit(fc,determinerCouleur(jcb.getSelectedIndex()));
			}
		});
		
		
		jcb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(dessin.getNbClic()!=1){
					fc=creeFigure(formes.getSelectedIndex());
					dessin.construit(fc,determinerCouleur(jcb.getSelectedIndex()));
				}else{
				}
					
			}
		});
		
	JPanel selectionFC = new JPanel();
	selectionFC.add(formes);
	selectionFC.add(jcb);
	
	this.add(selectionFC, BorderLayout.SOUTH);
		
		
	}
	
	public FigureColoree creeFigure(int nbc){
		FigureColoree res=null;
		switch (nbc) {
		case 0:
			res=new Quadrilatere();
			this.dessin.setNbClic(4);
			break;
		case 1:
			res=new Rectangle();
			
			this.dessin.setNbClic(2);
			break;
		case 2:
			res=new Triangle();
			this.dessin.setNbClic(3);
		}
		return res;
	}
	
	public Color determinerCouleur(int i){
		Color res=Color.BLACK;
		switch (i) {
		case 0:
			res=Color.BLACK;
			break;
		case 1:
			res=Color.BLUE;
			break;
		case 2:
			res=Color.YELLOW;
			break;
		case 3:
			res=Color.GREEN;
			break;
		case 4:
			res=Color.lightGray;
			break;
		case 5:
			res=Color.magenta;
			break;
		case 6:
			res=Color.pink;
			break;
		case 7:
			res=Color.RED;
			break;
		default:
			res=Color.BLACK;
		}
		return res;
	}

}
