package Controleur;

import java.awt.Color;
import java.awt.event.*;
import java.util.ArrayList;

import Modele.DessinModele;

public class ManipulateurFormes implements MouseListener,MouseMotionListener{

	private DessinModele model;
	int lastX,lastY;
	
	public ManipulateurFormes(DessinModele odm){
		this.model=odm;
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		int dx=e.getX()-lastX;
		int dy=e.getY()-lastY;
		for(int i=0;i<this.model.getLfg().size();i++){
			if(this.model.getLfg().get(i).isSelect()){
				this.model.getLfg().get(i).seTranslater(dx, dy);
			}
		}
		lastX=e.getX();
		lastY=e.getY();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if(this.model.getNbClic()==0){
			System.out.println("test");
			for(int i=0;i<this.model.getLfg().size();i++){
				if(this.model.estDedans(e.getX(), e.getY(),i)){
					if(!this.model.getLfg().get(i).isSelect())
						this.model.getLfg().get(i).selectionne();
					else
						this.model.getLfg().get(i).deSelectionne();
				}
			}
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		lastX=e.getX();
		lastY=e.getY();
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
