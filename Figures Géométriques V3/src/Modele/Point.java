package Modele;


public class Point {

	private int x,y;
	
	public Point(int xx , int yy){
		this.x=xx;
		this.y=yy;
	}
	
	public double distance(Point p){
		double res=0;
		res=this.x-p.x+this.y-p.y;
		return res;
	}
	
	public int rendreX(){
		return this.x;
	}
	
	public int rendreY(){
		return this.y;
	}
	
	public void incrementerX(int dx){
		this.x=this.x+dx;
	}
	
	public void incrementerY(int dy){
		this.y=this.y+dy;
	}
	
	public void modifierX(int mx){
		this.x=mx;
	}
	
	public void modifierY(int my){
		this.y=my;
	}
	
	public void translation(int dx,int dy){
		this.x=this.x+(dx/2);
		this.y=this.y+(dy/2);
	}
}

