package Modele;

import java.awt.*;

public abstract class FigureColoree {

	public static int TAILLE_CARRE_SELECTION;
	private boolean selected;
	private Color couleur;
	protected Point tab_mem[];
	
	public FigureColoree(){
		this.selected=false; 
		this.tab_mem = new Point[4];
		
	}
	
	public abstract int nbPoints();
	
	public abstract int nbClics();
	
	public abstract void modifierPoints(Point[] p);
	
	public abstract boolean estDedans(int x, int y);
	
	public abstract void seTranslater(int x, int y);
	
	public void affiche(Graphics g){
		if(this.selected)
			for(int i=0;i<this.tab_mem.length;i++){
				g.setColor(Color.BLACK);
				g.drawLine(this.tab_mem[i].rendreX()-5, this.tab_mem[i].rendreY(), this.tab_mem[i].rendreX()+5, this.tab_mem[i].rendreY());
				g.drawLine(this.tab_mem[i].rendreX(), this.tab_mem[i].rendreY()-5, this.tab_mem[i].rendreX(), this.tab_mem[i].rendreY()+5);
			}
	}
	
	public void selectionne(){
		this.selected=true;
	}
	
	public void deSelectionne(){
		this.selected=false;
	}
	
	public void changerCouleur(Color c){
		this.couleur=c;
	}
	
	public Color getColor(){
		return this.couleur;
	}
	
	public boolean isSelect(){
		return this.selected;
	}
}
