package Modele;

import java.awt.*;

public abstract class Polygone extends FigureColoree{
	
	protected Polygon p;	
	
	
	public Polygone(){
		
	}


	@Override
	public int nbClics() {
		// TODO Auto-generated method stub
		return 4;
	}
	

	@Override
	public void modifierPoints(Point[] pts) {
		int [] x = new int[pts.length];
		int [] y = new int[pts.length];
		for(int i=0; i<pts.length;i++){
			this.tab_mem[i]=pts[i];
			x[i] = pts[i].rendreX();
			y[i] = pts[i].rendreY();
			}
		this.p = new Polygon(x,y,pts.length);	
	}
	
	public void seTranslater(int x, int y){
		int [] px = new int[tab_mem.length];
		int [] py = new int[tab_mem.length];
		for(int i=0; i<tab_mem.length;i++){
			System.out.println("x = "+tab_mem[i].rendreX()+", dx = "+x);
			tab_mem[i].translation(x, y);
			tab_mem[i].translation(x, y);
			px[i] = tab_mem[i].rendreX();
			System.out.println("lx = "+tab_mem[i].rendreX());
			py[i] = tab_mem[i].rendreY();
			tab_mem[i]=new Point(px[i],py[i]);
		}
		this.p = new Polygon(px,py,tab_mem.length);
	}
	
	public void affiche(Graphics g){
		super.affiche(g);
		g.setColor(super.getColor());
		g.fillPolygon(p);
	}
	
	public boolean estDedans(int x, int y){
		boolean res=this.p.contains(x,y);
		return res;
	}


	
}
