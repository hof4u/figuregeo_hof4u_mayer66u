package Modele;

import java.awt.*;
import java.util.*;

public class DessinModele extends Observable{

	private int nbClic;
	private ArrayList<Point> points_cliques;
	private FigureColoree figureEnCours;
	private ArrayList<FigureColoree> lfg;
	
	public DessinModele(){
		this.lfg=new ArrayList<FigureColoree>();
		this.points_cliques=new ArrayList<Point>();
		this.nbClic=4;
		this.figureEnCours=new Quadrilatere();
	}
	
	public void ajoute(FigureColoree fc){
		lfg.add(fc);
		setChanged();

		notifyObservers();
		this.figureEnCours=null;
		this.points_cliques.clear();
		
	}
	
	public void changerCoul(FigureColoree fc,Color c){
		fc.changerCouleur(c);
	}
	
	public void construit(FigureColoree fc,Color c){
		this.figureEnCours=fc;
		this.figureEnCours.changerCouleur(c);
		this.figureEnCours.selectionne();
		setChanged();

		notifyObservers();
	}
	
	public void ajoutePt(int x, int y){
		this.points_cliques.add(new Point(x,y));
		setChanged();
		notifyObservers();
	}
	
	
	public int getNbClic(){
		return this.nbClic;
	}
	
	public void setNbClic(int nb){
		this.nbClic=nb;
	}
	
	public ArrayList<FigureColoree> getLfg(){
		setChanged();
		
		notifyObservers();
		return this.lfg;
	}
	
	public void setLfg(ArrayList<FigureColoree> lfc){
		this.lfg=lfc;
	}
	
	public FigureColoree getFigureEnCours(){
		return this.figureEnCours;
	}
	
	public void setFigureEnCours(FigureColoree fc){
		this.figureEnCours=fc;
	}
	
	public ArrayList<Point> getClics(){
		return this.points_cliques;
	}
	
	public boolean estDedans(int x, int y, int i){
		boolean res=false;
		res=this.lfg.get(i).estDedans(x, y);
		return res;
	}
	
	
	
	
}
