/**
 * Package correspondant a l'emplacement de la classe
 */
package Controleur;

/**
 * Importation des sources
 */
import java.awt.Color;
import java.awt.event.*;
import java.util.ArrayList;

import Modele.DessinModele;
import Modele.Point;

/**
 * Classe ManipulateurFormes, permettant la manipulation. Cette classe implement MouseListener et MouseMotionListener
 * 
 * @author MAYER Th�o, HOF Lucien
 */
public class ManipulateurFormes implements MouseListener,MouseMotionListener{

	/**
	 * Attribut model de type DessinModele
	 */
	private DessinModele model;
	/**
	 * Attributs lastX et lastY de type int
	 */
	int lastX,lastY;
	/**
	 * Attribut p de type boolean
	 */
	public boolean p;
	
	/**
	 * Constructeur de la classe ManipulateurFormes, permettant d initialiser le manipulateur
	 * 
	 * @param odm
	 */
	public ManipulateurFormes(DessinModele odm){
		this.model=odm;
		p=false;
	}
	
	/**
	 * Methode mouseDragged
	 * 
	 * @param e
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		if(this.model.getType()=="manip"){
			int dx=e.getX()-lastX;
			int dy=e.getY()-lastY;
			for(int i=0;i<this.model.getLfg().size();i++){
				if(this.model.getLfg().get(i).isSelect()){
					if(p){
						for(int j=0;j<this.model.getLfg().get(i).getTab().length;j++){
							if(this.model.getLfg().get(i).getTab()[j].isSelect()){
								Point pts[]=this.model.getLfg().get(i).getTab();
								Point nouv = new Point(e.getX(), e.getY());
								nouv.selectionner();
								pts[j]=nouv;
								this.model.getLfg().get(i).modifierPoints(pts);
							}
						}
					}else{
						this.model.getLfg().get(i).seTranslater(dx, dy);
					}
				}
			}
			lastX=e.getX();
			lastY=e.getY();
		}
	}

	/**
	 * Methode mouseMoved
	 * 
	 * @param e
	 */
	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub	
	}

	/**
	 * Methode mouseClicked
	 * 
	 * @param e
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		if(this.model.getType()=="manip"){
			for(int i=0;i<this.model.getLfg().size();i++){
				if(this.model.estDedans(e.getX(), e.getY(),i)){
					if(!this.model.getLfg().get(i).isSelect()){
						this.model.getLfg().get(i).selectionne();
					}else
						this.model.getLfg().get(i).deSelectionne();
				}
			}
		}
		
	}

	/**
	 * Methode mouseEntered
	 * 
	 * @param arg0
	 */
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Methode mouseExited
	 * 
	 * @param arg0
	 */
	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Methode mousePressed
	 * 
	 * @param e
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		lastX=e.getX();
		lastY=e.getY();
		if(this.model.getType()=="manip"){
			for(int i=0;i<this.model.getLfg().size();i++){
				if(this.model.getLfg().get(i).isSelect()){
					for(int j=0;j<this.model.getLfg().get(i).getTab().length;j++){
						if(this.model.getLfg().get(i).getTab()[j].estSur(e.getX(), e.getY())){
							this.model.getLfg().get(i).getTab()[j].selectionner();
							p=true;
						}
					}
				}
			}
		}
		
	}

	/**
	 * Methode mouseReleased
	 * 
	 * @param arg0
	 */
	@Override
	public void mouseReleased(MouseEvent arg0) {
		if(this.model.getType()=="manip"){
			for(int i=0;i<this.model.getLfg().size();i++){
				for(int j=0;j<this.model.getLfg().get(i).getTab().length;j++){
					this.model.getLfg().get(i).getTab()[j].deSelectionner();
				}
			}
			p=false;
			
		}
	}

}
