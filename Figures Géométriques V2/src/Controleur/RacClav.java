/**
 * Package correspondant a l'emplacement de la classe
 */
package Controleur;

/**
 * Importation des sources
 */
import java.awt.event.*;

import javax.print.attribute.Size2DSyntax;

import Modele.DessinModele;
import Modele.FigureColoree;

/**
 * Classe RacClav, permettant de gerer les raccourcis clavier. Cette classe implement KeyListener
 * 
 * @author MAYER Th�o, HOF Lucien
 */
public class RacClav implements KeyListener{
	
	/**
	 * Attribut dessin de type DessinModele
	 */
	private DessinModele dessin;
	
	/**
	 * Constructeur de la classe RacClav, permettant d initialiser un dessin modele
	 * 
	 * @param d
	 */
	public RacClav(DessinModele d){
		this.dessin=d;
	}

	/**
	 * Methode keyPressed
	 * 
	 * @param e
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		int r=e.getKeyCode();
		if((e.getKeyCode() == KeyEvent.VK_Z) && ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0)){
			switch(dessin.getCtrlZ()){
			case "efface":
				dessin.setFigureZ(dessin.getLfg().get(dessin.getLfg().size()-1));
				if(dessin.getLfg().size()!=0)
					dessin.getLfg().remove(dessin.getLfg().size()-1);
				dessin.setCtrlZ("effaceAnnule");
				break;
			case "effaceAnnule":
				dessin.ajoute(dessin.getFigureZ());
				dessin.setCtrlZ("efface");
				break;
			case "restaure":
				FigureColoree temp = dessin.getFigureZ();
				dessin.setFigureZ(dessin.getLfg().get(dessin.getLfg().size()-1));
				dessin.getLfg().remove(dessin.getLfg().get(dessin.getLfg().size()-1));
				dessin.getLfg().add(temp); 
				break;
			}
			System.out.println("crtl + z");
		}
		
	}

	/**
	 * Methode keyReleased
	 * 
	 * @param arg0
	 */
	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Methode keyTyped
	 * 
	 * @param arg0
	 */
	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
