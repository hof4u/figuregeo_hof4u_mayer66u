/**
 * Package correspondant a l'emplacement de la classe
 */
package Controleur;

/**
 * Importation des sources
 */
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import Modele.*;
import Modele.Point;
import Modele.Rectangle;

/**
 * Classe PanneauChoix, permettant de gerer tous les differents buttons. Cette classe implement ChangeListener
 * 
 * @author MAYER Th�o, HOF Lucien
 */
public class PanneauChoix extends JPanel implements ChangeListener{
	
	/**
	 * Attribut fc de type FigureColoree
	 */
	private FigureColoree fc;
	/**
	 * Attribut dessin de type DessinModele
	 */
	private DessinModele dessin;
	/**
	 * Attribut jcb de type JComboBox
	 */
	private JComboBox jcb;
	/**
	 * Attribut formes de type JComboBox
	 */
	private JComboBox formes;
	/**
	 * Attribut ep de type JComboBox
	 */
	private JComboBox ep;
	/**
	 * Attribut cons de type JButton
	 */
	private JButton cons;
	/**
	 * Attribut tcc de type JColorChooser
	 */
	private JColorChooser tcc;
	/**
	 * Attribut repCoul de type JTextField
	 */
	private JTextField  repCoul;
	/**
	 * Attribut lastUse de type boolean
	 */
	private boolean lastUse;
	
	/**
	 * Constructeur de la classe PanneauChoix, permettant d initialiser le panneau choix
	 * 
	 * @param des
	 */
	public PanneauChoix(DessinModele des){
		this.setLayout(new BorderLayout());
		this.dessin=des;
		JRadioButton nf = new JRadioButton("Nouvelle Figure");
		JRadioButton tm = new JRadioButton("Trace � main lev�e");
		JRadioButton man = new JRadioButton("Manipulation");
		jcb = new JComboBox(new String[] { "Noir", "Bleu", "Jaune", "Vert","Gris","Violet","Rose","Rouge" });
		this.formes = new JComboBox(new String[] { "Quadrilat�re" , "Rectangle" , "Triangle","Carr�","Losange","Cercle","Ellipse","Polygone"});
		this.ep = new JComboBox(new String[] { "1px","3px","5px","7px"});
		JButton effacer = new JButton("Effacer figures");
		this.cons = new JButton("Construire Polygone");
		JButton suprSelec = new JButton("Effacer figure selectionn�e");
		this.tcc = new JColorChooser();
		
		JButton colorChooser = new JButton("Couleurs");
		tcc = new JColorChooser(this.getForeground());
	    tcc.getSelectionModel().addChangeListener(this);
	    tcc.setBorder(BorderFactory.createTitledBorder("Choose Text Color"));
		
		
		fc=creeFigure(formes.getSelectedIndex());
		dessin.construit(fc, determinerCouleur(jcb.getSelectedIndex()));
		ep.setEnabled(false);
		
		effacer.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				dessin.getLfg().clear();
				
			}
			
			
		});
		
		suprSelec.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
				for(int i = 0; i<dessin.getLfg().size();i++){
					if(dessin.getLfg().get(i).isSelect()){
						dessin.getLfg().remove(i);
					}
				}
			}
		});
		
		nf.setSelected(true);
		nf.addActionListener(new ActionListener(){

			
			
			@Override
			public void actionPerformed(ActionEvent e) {
				formes.setEnabled(true);
				jcb.setEnabled(true);
				ep.setEnabled(false);
				for(int i=0; i<dessin.getLfg().size();i++){
					dessin.getLfg().get(i).deSelectionne();
				}
				fc=creeFigure(formes.getSelectedIndex());
				if(!lastUse){
					dessin.construit(fc,determinerCouleur(jcb.getSelectedIndex()));
				}else{
					dessin.construit(fc,tcc.getColor());
				}
				cons.setEnabled(false);
				
						
				
			}
		});
		
		tm.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				formes.setEnabled(false);
				jcb.setEnabled(true);
				ep.setEnabled(true);
				dessin.setType("trace");
				for(int i=0; i<dessin.getLfg().size();i++){
					dessin.getLfg().get(i).deSelectionne();
				}
				fc=new Trace();
				
				if(!lastUse){
					dessin.construit(fc,determinerCouleur(jcb.getSelectedIndex()));
				}else{
					
					dessin.construit(fc,tcc.getColor());
				}	
				cons.setEnabled(false);
				ep.setSelectedIndex(0);
				
			}
		});
		
		man.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e) {
				formes.setEnabled(false);
				jcb.setEnabled(true);
				ep.setEnabled(false);
				for(int i=0; i<dessin.getLfg().size();i++){
					dessin.getLfg().get(i).deSelectionne();
				}
				dessin.setType("manip");	
				cons.setEnabled(false);
				
			}
		});
		
		cons.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Point pts[] = new Point[dessin.getClics().size()];
				for(int i=0;i<pts.length;i++){
					pts[i]=dessin.getClics().get(i);
				}
				dessin.getFigureEnCours().modifierPoints(pts);
				((PolygoneQuelconque) dessin.getFigureEnCours()).setFini(true);
				dessin.getFigureEnCours().deSelectionne();
				Color c = dessin.getFigureEnCours().getColor();
				dessin.ajoute(dessin.getFigureEnCours());
				FigureColoree fc= creeFigure(formes.getSelectedIndex());
				dessin.construit(fc,c);
			}
		});
		
		ButtonGroup bg = new ButtonGroup();
		bg.add(nf);
		bg.add(tm);
		bg.add(man);
		JPanel typeDessin = new JPanel();
		
		typeDessin.add(nf);
		typeDessin.add(tm);
		typeDessin.add(man);
		
		this.add(typeDessin, BorderLayout.NORTH);
		
		
		formes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fc=creeFigure(formes.getSelectedIndex());
				if(!lastUse){
					dessin.construit(fc,determinerCouleur(jcb.getSelectedIndex()));
				}else{
					dessin.construit(fc,tcc.getColor());
				}
			}
		});
		
		
		jcb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lastUse=false;
				repCoul.setBackground(determinerCouleur(jcb.getSelectedIndex()));
				if(dessin.getType()=="manip"){
					for(int i=0;i<dessin.getLfg().size();i++){
						if(dessin.getLfg().get(i).isSelect()){
							dessin.getLfg().get(i).changerCouleur(determinerCouleur(jcb.getSelectedIndex()));
						}
					}
				}else{
					if(dessin.getType()!="trace"){
						fc=creeFigure(formes.getSelectedIndex());
						dessin.construit(fc,determinerCouleur(jcb.getSelectedIndex()));
					}else{
						fc=getEpaisseur(ep.getSelectedIndex());
						dessin.construit(fc,determinerCouleur(jcb.getSelectedIndex()));
					}
				}
					
			}
		});
		
		ep.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(dessin.getType()=="manip"){
					for(int i=0;i<dessin.getLfg().size();i++){
						if(dessin.getLfg().get(i).isSelect()){
							dessin.getLfg().get(i).changerCouleur(determinerCouleur(jcb.getSelectedIndex()));
						}
					}
				}else{
					if(dessin.getType()!="trace"){
						fc=creeFigure(formes.getSelectedIndex());
						if(!lastUse){
							dessin.construit(fc,determinerCouleur(jcb.getSelectedIndex()));
						}else{
							
							dessin.construit(fc,tcc.getColor());
						}	
					}else{
						fc=getEpaisseur(ep.getSelectedIndex());			
						if(!lastUse){
							dessin.construit(fc,determinerCouleur(jcb.getSelectedIndex()));
						}else{
							
							dessin.construit(fc,tcc.getColor());
						}	
					}
				}
			}
		});
		
		colorChooser.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFrame couleur = new JFrame("Couleurs");
				couleur.setVisible(true);
				couleur.setPreferredSize(new Dimension(450,300));
				couleur.pack();
				couleur.requestFocusInWindow();
				couleur.add(tcc);
				
			}
			
		});
		
	repCoul = new JTextField("     ");
	
	
	JPanel selectionFC = new JPanel();
	selectionFC.add(formes);
	selectionFC.add(jcb);
	selectionFC.add(colorChooser);
	selectionFC.add(repCoul);
	selectionFC.add(ep);
	selectionFC.add(cons);
	
	this.add(selectionFC, BorderLayout.CENTER);
	
	JPanel eff = new JPanel();
	eff.add(effacer);
	eff.add(suprSelec);
	this.add(eff, BorderLayout.SOUTH);
		
		
	}
	
	/**
	 * Methode creeFigure, permettant de creer les differentes figures
	 * 
	 * @param nbc
	 * @return res
	 */
	public FigureColoree creeFigure(int nbc){
		FigureColoree res=null;
		switch (nbc) {
		case 0:
			res=new Quadrilatere();
			this.dessin.setType("quad");
			this.cons.setEnabled(false);
			break;
		case 1:
			res=new Rectangle();
			this.dessin.setType("rec");
			this.cons.setEnabled(false);
			break;
		case 2:
			res=new Triangle();
			this.dessin.setType("tri");
			this.cons.setEnabled(false);
			break;
		case 3:
			res=new Carre();
			this.dessin.setType("carre");
			this.cons.setEnabled(false);
			break;
		case 4:
			res=new Losange();
			this.dessin.setType("los");
			this.cons.setEnabled(false);
			break;
		case 5:
			res=new Cercle();
			this.dessin.setType("cer");
			this.cons.setEnabled(false);
			break;
		case 6:
			res=new Ellipse();
			this.dessin.setType("eli");
			this.cons.setEnabled(false);
			break;
		case 7: 
			res=new PolygoneQuelconque();
			this.dessin.setType("poly");
			this.cons.setEnabled(true);
			break;
		}
		
		return res;
	}
	
	/**
	 * Methode determinerCouleur, permettant de determiner la couleur d une figure
	 * 
	 * @param i
	 * @return res
	 */
	public Color determinerCouleur(int i){
		Color res=Color.BLACK;
		switch (i) {
		case 0:
			res=Color.BLACK;
			break;
		case 1:
			res=Color.BLUE;
			break;
		case 2:
			res=Color.YELLOW;
			break;
		case 3:
			res=Color.GREEN;
			break;
		case 4:
			res=Color.lightGray;
			break;
		case 5:
			res=Color.magenta;
			break;
		case 6:
			res=Color.pink;
			break;
		case 7:
			res=Color.RED;
			break;
		default:
			res=Color.BLACK;
		}
		return res;
	}

	/**
	 * Methode stateChanged, permettant de changer de choix, de Nouvelle Figure a Manip par exemple
	 * 
	 * @param arg0
	 */
	@Override
	public void stateChanged(ChangeEvent arg0) {
		this.lastUse = true;
		repCoul.setBackground(tcc.getColor());
		if(dessin.getType()=="manip"){
			for(int i=0;i<dessin.getLfg().size();i++){
				if(dessin.getLfg().get(i).isSelect()){
					dessin.getLfg().get(i).changerCouleur(tcc.getColor());
				}
			}
		}else{
			if(dessin.getType()!="trace"){
				fc=creeFigure(formes.getSelectedIndex());
				dessin.construit(fc,tcc.getColor());
			}else{
				fc=getEpaisseur(ep.getSelectedIndex());
				dessin.construit(fc,tcc.getColor());
			}
		}
	}
	
	/**
	 * Methode getEpaisseur, permettant d obtenir l epaisseur du trait
	 * 
	 * @param i
	 * @return fc
	 */
	public Trace getEpaisseur(int i){
		Trace fc=new Trace();
		switch(i){
		case 0:
				(fc).setEp(1);
			break;
		case 1:
				(fc).setEp(3);
			break;
		case 2:
				(fc).setEp(5);
			break;
		case 3:
				(fc).setEp(7);
			break;
		}
		return fc;
	
	}
	
	

}
