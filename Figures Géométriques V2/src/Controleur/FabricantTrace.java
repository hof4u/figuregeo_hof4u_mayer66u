/**
 * Package correspondant a l'emplacement de la classe
 */
package Controleur;

/**
 * Importation des sources
 */
import java.awt.Color;
import java.awt.event.*;
import java.util.*;

import Modele.*;

/**
 * Classe FabricantTrace, permettant la creation d une trace. Cette classe implement MouseListener et MouseMotionListener
 * 
 * @author MAYER Th�o, HOF Lucien
 */
public class FabricantTrace implements MouseListener, MouseMotionListener{
	
	/**
	 * Attribut model de type DessinModele
	 */
	private DessinModele model;
	/**
	 * Attribut t de type Trace
	 */
	private Trace t;
	/**
	 * Attribut p de type Point
	 */
	private Point p[];
	/**
	 * Attributs lX et lY de type int
	 */
	private int lX,lY;
	/**
	 * Attribut count de type int
	 */
	public int count;
	/**
	 * Attribut ep de type int
	 */
	private int ep;
	
	/**
	 * Constructeur de la classe FabricantTrace, permettant d initialiser le fabricanttrace
	 * 
	 * @param odm
	 */
	public FabricantTrace(DessinModele odm){
		this.model = odm;
		p=new Point[2];
		this.count=0;
		//ep=1;
	}

	/**
	 * Methode mouseClicked
	 * 
	 * @param arg0
	 */
	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Methode mouseEntered
	 * 
	 * @param arg0
	 */
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Methode mouseExited
	 * 
	 * @param arg0
	 */
	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Methode mousePressed
	 * 
	 * @param e
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		if(model.getType()=="trace"){
			t=(Trace) model.getFigureEnCours();
			lX=e.getX();
			lY=e.getY();
		}
	}

	/**
	 * Methode mouseReleased
	 * 
	 * @param arg0
	 */
	@Override
	public void mouseReleased(MouseEvent arg0) {
		if(model.getType()=="trace"){
			Color c = t.getColor();
			ep=t.getEp();
			Trace fc=new Trace();
			fc.setEp(ep);
			model.construit(fc, c);
		}
		this.count=0;
		
	}

	/**
	 * Methode mouseDragged
	 * 
	 * @param e
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		if(model.getType()=="trace"){
			p[0]=new Point(lX,lY);
			p[1]=new Point(e.getX(),e.getY());
			t.modifierPoints(p);
			lX=e.getX();
			lY=e.getY();
			if(count>0)
				model.suprLast();
			model.ajoute(t);
			
			count++;
		}
		
	}

	/**
	 * Methode mouseMoved
	 * 
	 * @param arg0
	 */
	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
