/**
 * Package correspondant a l'emplacement de la classe
 */
package Controleur;

/**
 * Importation des sources
 */
import java.awt.Color;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

import Modele.*;

/**
 * Classe FabricantFigures permettant de creer les differentes figures. Cette classe implement MouseListener et MouseMotionListener
 * 
 * @author MAYER Th�o, HOF Lucien
 */
public class FabricantFigures implements MouseListener,MouseMotionListener{

	/**
	 * Attibut model de type DessinModele
	 */
	private DessinModele model;
	/**
	 * Attibut count de type int
	 */
	private int count=0;
	/**
	 * Attibut pq de type Point
	 */
	private Point pq[] ;
	/**
	 * Attibut pr de type Point
	 */
	private Point pr[] ;
	/**
	 * Attibut pt de type Point
	 */
	private Point pt[] ;
	/**
	 * Attibut c de type Color
	 */
	private Color c=Color.BLACK;
	/**
	 * Attibuts lastX et lastY de type int
	 */
	int lastX,lastY;
	/**
	 * Attibut cer de type Cercle
	 */
	private Cercle cer;
	/**
	 * Attibut rec de type Rectangle
	 */
	private Rectangle rec;
	/**
	 * Attibut car de type Carre
	 */
	private Carre car;
	/**
	 * Attibut los de type Losange
	 */
	private Losange los;
	/**
	 * Attibut eli de type Ellipse
	 */
	private Ellipse eli;
	
	/**
	 * Methode mouseClicked
	 * 
	 * @param e
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
	}

	/**
	 * Methode mouseEntered
	 * 
	 * @param arg0
	 */
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}
	
	/**
	 * Methode mouseDragged
	 * 
	 * @param e
	 */
	public void mouseDragged(MouseEvent e) {
		if(this.model.getType()=="cer"){
			this.model.ajoutePt(lastX, lastY);
			pr= new Point[]{new Point(lastX,lastY),new Point(e.getX(),e.getY())};
			cer.modifierPoints(pr);
			c=cer.getColor();
			this.model.construit(cer,c);
		}
		if(this.model.getType()=="eli"){
			this.model.ajoutePt(lastX, lastY);
			pr= new Point[]{new Point(lastX,lastY),new Point(e.getX(),e.getY())};
			eli.modifierPoints(pr);
			c=eli.getColor();
			this.model.construit(eli,c);
		}
		if(this.model.getType()=="rec"){
			this.model.ajoutePt(lastX, lastY);
			pr= new Point[]{new Point(lastX,lastY),new Point(e.getX(),e.getY())};
			rec.modifierPoints(pr);
			c=rec.getColor();
			this.model.construit(rec,c);
		}
		if(this.model.getType()=="carre"){
			this.model.ajoutePt(lastX, lastY);
			pr= new Point[]{new Point(lastX,lastY),new Point(e.getX(),e.getY())};
			car.modifierPoints(pr);
			c=car.getColor();
			this.model.construit(car,c);
		}
		if(this.model.getType()=="los"){
			this.model.ajoutePt(lastX, lastY);
			pr= new Point[]{new Point(lastX,lastY),new Point(e.getX(),e.getY())};
			los.modifierPoints(pr);
			c=los.getColor();
			this.model.construit(los,c);
		}
	}

	/**
	 * Methode mouseExited
	 * 
	 * @param arg0
	 */
	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}
	
	/**
	 * Methode mouseReleased
	 * 
	 * @param arg0
	 */
	@Override
	public void mouseReleased(MouseEvent arg0) {
		if(this.model.getType()=="cer"){
			cer.deSelectionne();
			cer.setFini(true);
			this.model.ajoute(cer);
			this.model.construit(new Cercle(),c);
		}
		if(this.model.getType()=="eli"){
			eli.deSelectionne();
			eli.setFini(true);
			this.model.ajoute(eli);
			this.model.construit(new Ellipse(),c);
		}
		if(this.model.getType()=="rec"){
			rec.deSelectionne();
			rec.setFini(true);
			this.model.ajoute(rec);
			this.model.construit(new Rectangle(),c);
		}
		if(this.model.getType()=="carre"){
			car.deSelectionne();
			car.setFini(true);
			this.model.ajoute(car);
			this.model.construit(new Carre(),c);
		}
		if(this.model.getType()=="los"){
			los.deSelectionne();
			los.setFini(true);
			this.model.ajoute(los);
			this.model.construit(new Losange(),c);
		}
	}
	
	/**
	 * Constructeur de la classe FabricantFigures, permettant d initialiser le fabricantfigures
	 * 
	 * @param odm
	 */
	public FabricantFigures(DessinModele odm){
		this.model=odm;
		pq = new Point[4];
		pr = new Point[2];
		pt = new Point[3];
		
	}

	/**
	 * Methode mousePressed
	 * 
	 * @param e
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		if(this.model.getType()=="quad"){
			int x=e.getX();
			int y=e.getY();
			this.model.ajoutePt(x, y);
			pq[count]=new Point(x,y);
			this.count++;
			if(count==4){
				FigureColoree fc;
				fc = this.model.getFigureEnCours();
				fc.modifierPoints(pq);
				fc.deSelectionne();
				c=fc.getColor();
				this.model.ajoute(fc);
				this.model.construit(new Quadrilatere(),c);
				this.count=0;
			}
		}
		
		if(this.model.getType()=="tri"){
			int x=e.getX();
			int y=e.getY();
			this.model.ajoutePt(x, y);
			pt[count]=new Point(x,y);
			this.count++;
			if(count==3){
				FigureColoree fc;
				fc = this.model.getFigureEnCours();
				fc.modifierPoints(pt);
				fc.deSelectionne();
				c=fc.getColor();
				((Polygone) fc).setFini(true);
				this.model.ajoute(fc);
				this.model.construit(new Triangle(),c);
				this.count=0;
			}
		}
		
		if(this.model.getType()=="rec"){
			rec = (Rectangle) this.model.getFigureEnCours();
			this.lastX=e.getX();
			this.lastY=e.getY();
		}
		
		if(this.model.getType()=="carre"){
			car = (Carre) this.model.getFigureEnCours();
			this.lastX=e.getX();
			this.lastY=e.getY();
		}
		
		if(this.model.getType()=="los"){
			los = (Losange) this.model.getFigureEnCours();
			this.lastX=e.getX();
			this.lastY=e.getY();
		}
		
		if(this.model.getType()=="cer"){
			cer = (Cercle) this.model.getFigureEnCours();
			this.lastX=e.getX();
			this.lastY=e.getY();
		}
		
		if(this.model.getType()=="eli"){
			eli = (Ellipse) this.model.getFigureEnCours();
			this.lastX=e.getX();
			this.lastY=e.getY();
		}
		
		if(this.model.getType()=="poly"){
			this.model.getClics().add(new Point(e.getX(),e.getY()));
		}	
	}

	/**
	 * Methode mouseMoved
	 * 
	 * @param arg0
	 */
	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}



}
