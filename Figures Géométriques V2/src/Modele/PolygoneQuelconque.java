/**
 * Package correspondant a l'emplacement de la classe
 */
package Modele;

/**
 * Importation des sources
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

/**
 * Classe PolgoneQuelconque permettant de creer un polygone quelconque, elle extend de FigureColoree
 * 
 * @author MAYER Th�o, HOF Lucien
 */
public class PolygoneQuelconque extends FigureColoree{

	/**
	 * Attribut p de type Polygon
	 */
	protected Polygon p;
	/**
	 * Attribut fini de type boolean
	 */
	protected boolean fini;
	
	/**
	 * Constructeur de la classe PolygoneQuelconque, permettant d initialiser un polygone quelconque
	 */
	public PolygoneQuelconque(){
		p=new Polygon();
		this.fini=false;
		this.tab_mem=new Point[4];
	}
	
	/**
	 * Methode nbPoints, permettant de savoir le nombre de points pour un polygone quelconque
	 * 
	 * @return 0
	 */
	@Override
	public int nbPoints() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * Methode nbClics, permettant de savoir le nombre de clics pour un polygone quelconque
	 * 
	 * @return 0
	 */
	@Override
	public int nbClics() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * Methode modifierPoints, permettant de modifier les points d un polygone quelconque
	 * 
	 * @param pts
	 */
	@Override
	public void modifierPoints(Point[] pts) {
		this.tab_mem=new Point[pts.length];
		int [] x = new int[pts.length];
		int [] y = new int[pts.length];
		for(int i=0; i<pts.length;i++){
			this.tab_mem[i]=pts[i];
			x[i] = pts[i].rendreX();
			y[i] = pts[i].rendreY();
			}
		this.p = new Polygon(x,y,pts.length);
		
	}

	/**
	 * Methode estDedans, permettant de savoir si la souris est dans le polygone quelconque
	 * 
	 * @param x
	 * @param y
	 * 
	 * @return res
	 */
	@Override
	public boolean estDedans(int x, int y) {
		boolean res=this.p.contains(x,y);
		return res;
		}

	/**
	 * Methode seTranslater, permettant de translater le polygone quelconque
	 * 
	 * @param x
	 * @param y
	 */
	@Override
	public void seTranslater(int x, int y) {
		int [] px = new int[tab_mem.length];
		int [] py = new int[tab_mem.length];
		for(int i=0; i<tab_mem.length;i++){
			tab_mem[i].translation(x, y);
			px[i] = tab_mem[i].rendreX();
			py[i] = tab_mem[i].rendreY();
			tab_mem[i]=new Point(px[i],py[i]);
		}
		this.p = new Polygon(px,py,tab_mem.length);
	}
	
	/**
	 * Methode setFini, permettant de set un boolean a fini
	 * 
	 * @param f
	 */
	public void setFini(boolean f){
		this.fini=f;
	}
	
	/** 
	 * Methode affiche, permettant d afficher le polygone quelconque
	 * 
	 * @param g
	 */
	public void affiche(Graphics g){
		if(fini){
			g.setColor(super.getColor());
			g.fillPolygon(p);
			super.affiche(g);
		}else{
			g.setColor(Color.BLACK);
			g.drawPolygon(p);
		}
	}

}
