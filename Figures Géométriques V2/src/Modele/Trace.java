/**
 * Package correspondant a l'emplacement de la classe
 */
package Modele;

/**
 * Importation des sources
 */
import java.awt.*;
import java.util.*;
import javax.swing.plaf.synth.SynthSplitPaneUI;

/**
 * Classe Trace, permettant de creer un trac� a main leve. Cette classe extend de FigureColoree
 * 
 * @author MAYER Th�o, HOF Lucien
 */
public class Trace extends FigureColoree{
	
	/**
	 * Attribut trace de type ArrayList Point
	 */
	public ArrayList<Point> trace;
	/**
	 * Attribut selec de type int, initialement a 10
	 */
	public static int selec=10;
	/**
	 * Attribut epaisseur de type int
	 */
	public int epaisseur;
	
	/**
	 * Constructeur de la classe Trace, permettant d initialiser la trace
	 */
	public Trace(){
		trace=new ArrayList<Point>();
		this.epaisseur=1;
	}

	/**
	 * Methode nbPoints, permettant d obtenir le nombre de points pour un trait
	 * 
	 * @return this.trace.size()
	 */
	@Override
	public int nbPoints() {
		// TODO Auto-generated method stub
		return this.trace.size();
	}

	/**
	 * Methode nbClics, permettant d obtenir le nombre de clics pour un trait
	 * 
	 * @return 0
	 */
	@Override
	public int nbClics() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * Methode modifierPoints, permettant de modifier les points du trait
	 * 
	 * @param p
	 */
	@Override
	public void modifierPoints(Point[] p) {
		for(int i=0;i<p.length;i++){
			trace.add(p[i]);
		}
	}
	
	/**
	 * Methode setEp, permettant de set une epaisseur au trait
	 * 
	 * @param e
	 */
	public void setEp(int e){
		this.epaisseur=e;
	}
	
	/**
	 * Methode getEp, permettant d obtenir l epaisseur du trait
	 * 
	 * @return this.epaisseur
	 */
	public int getEp(){
		return this.epaisseur;
	}

	/**
	 * Methode estDedans, permettant de savoir si la souris est sur le trait
	 * 
	 * @param x
	 * @param y
	 * 
	 * @return res
	 */
	@Override
	public boolean estDedans(int x, int y) {
		boolean res=false;
		for(int i=0;i<this.trace.size();i++){
			if(this.trace.get(i).distance(new Point(x,y))<=selec){
				res=true;
			}
		}
		return res;
	}

	/**
	 * Methode seTranslater, permettant de deplacer le trait
	 * 
	 * @param x
	 * @param y
	 */
	@Override
	public void seTranslater(int x, int y) {
		for(int i=0; i<trace.size();i++){
			trace.get(i).translation(x, y);
		}
		
	}
	
	/**
	 * Methode affiche, permettant d afficher le trait
	 * 
	 * @param g
	 */
	public void affiche(Graphics g){
		if(super.selected){
			g.setColor(Color.BLACK);
			if(this.trace!=null){
				if(this.trace.get(0)!=null){
					g.drawLine(this.trace.get(0).rendreX()-5, this.trace.get(0).rendreY(), this.trace.get(0).rendreX()+5, this.trace.get(0).rendreY());
					g.drawLine(this.trace.get(0).rendreX(), this.trace.get(0).rendreY()-5, this.trace.get(0).rendreX(), this.trace.get(0).rendreY()+5);
					g.drawLine(this.trace.get(this.trace.size()-1).rendreX()-5, this.trace.get(this.trace.size()-1).rendreY(), this.trace.get(this.trace.size()-1).rendreX()+5, this.trace.get(this.trace.size()-1).rendreY());
					g.drawLine(this.trace.get(this.trace.size()-1).rendreX(), this.trace.get(this.trace.size()-1).rendreY()-5, this.trace.get(this.trace.size()-1).rendreX(), this.trace.get(this.trace.size()-1).rendreY()+5);
				}
			}
		}
		Graphics2D g2 = (Graphics2D) g;
		g2.setStroke(new BasicStroke(epaisseur));
		g2.setColor(super.getColor());
		for(int i=0; i<trace.size()-1;i++){
			g2.drawLine(trace.get(i).rendreX(), trace.get(i).rendreY(), trace.get(i+1).rendreX(), trace.get(i+1).rendreY());
		}
		g2.setStroke(new BasicStroke(1));
	}

	
}
