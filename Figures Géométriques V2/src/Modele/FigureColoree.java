/**
 * Package correspondant a l'emplacement de la classe
 */
package Modele;

/**
 * Importation des sources
 */
import java.awt.*;

/**
 * Classe abstract FigureColoree qui permet de creer la figure
 * 
 * @author MAYER Th�o, HOF Lucien
 */
public abstract class FigureColoree {
	
	/**
	 * Attribut TAILLE_CARRE_SELECTION de type int
	 */
	public static int TAILLE_CARRE_SELECTION;
	/**
	 * Attribut selected de type boolean
	 */
	protected boolean selected;
	/**
	 * Attribut couleur de type Color
	 */
	private Color couleur;
	/**
	 * Attribut tab_mem[] de type Point
	 */
	protected Point tab_mem[];
	
	
	/**
	 * Constructeur de la classe FigureColoree qui permet d initialiser une figure
	 */
	public FigureColoree(){
		this.selected=false; 
		this.tab_mem = new Point[] {new Point(0,0),new Point(0,0),new Point(0,0),new Point(0,0)};
		
	}
	
	/**
	 * Methode abstract nbPoints, permettant de connaitre le nombre de points
	 * 
	 * @return int
	 */
	public abstract int nbPoints();
	
	/**
	 * Methode abstract nbClics, permettant de connaitre le nombre de clics
	 * 
	 * @return int
	 */
	public abstract int nbClics();
	
	/**
	 * Methode abstract modifierPoint, permettant de modifier un point
	 * 
	 * @param p
	 */
	public abstract void modifierPoints(Point[] p);
	
	/**
	 * Methode abstract estDedans, permettant de savoir si le pointeur de la souris est dans la figure
	 * 
	 * @param x
	 * @param y
	 * @return boolean
	 */
	public abstract boolean estDedans(int x, int y);
	
	/**
	 * Methode abstract seTranslater, permettant de translater une figure
	 * 
	 * @param x
	 * @param y
	 */
	public abstract void seTranslater(int x, int y);
	
	/**
	 * Methode affiche, permettant d afficher la figure
	 * 
	 * @param g
	 */
	public void affiche(Graphics g){
		if(this.selected){
			if(this instanceof Cercle==false && this instanceof Trace==false){
				for(int i=0;i<this.tab_mem.length;i++){
					if(this.tab_mem[i]!=null){
						g.setColor(Color.BLACK);
						if(this.couleur==Color.BLACK)
							g.setColor(Color.RED);
						g.fillOval(this.tab_mem[i].rendreX()-4, this.tab_mem[i].rendreY()-4, 8, 8);
					}
				}
			}else{
				g.setColor(Color.BLACK);
				g.drawLine(this.tab_mem[0].rendreX()-5, this.tab_mem[0].rendreY(), this.tab_mem[0].rendreX()+5, this.tab_mem[0].rendreY());
				g.drawLine(this.tab_mem[0].rendreX(), this.tab_mem[0].rendreY()-5, this.tab_mem[0].rendreX(), this.tab_mem[0].rendreY()+5);
			}
			
		}
	}
	
	/**
	 * Methode selectionne, permettant de selectionner une figure
	 */
	public void selectionne(){
		this.selected=true;
	}
	
	/**
	 * Methode deSelectionne, permettant de deselectionner une figure
	 */
	public void deSelectionne(){
		this.selected=false;
	}
	
	/**
	 * Methode changerCouleur, permettant de changer la couleur de la figure
	 * 
	 * @param c
	 */
	public void changerCouleur(Color c){
		this.couleur=c;
	}
	
	/**
	 * Methode getColor, permettant d obtenir la couleur de la figure
	 * 
	 * @return this.couleur
	 */
	public Color getColor(){
		return this.couleur;
	}
	
	/**
	 * Methode isSelect, permettant de savoir si la figure est selectionne
	 * 
	 * @return this.selected
	 */
	public boolean isSelect(){
		return this.selected;
	}
	
	/**
	 * Methode getTab, permettant d obtenir le tableau de point
	 * 
	 * @return this.tab_mem
	 */
	public Point[] getTab(){
		return this.tab_mem;
	}
}
