/**
 * Package correspondant a l'emplacement de la classe
 */
package Modele;

/**
 * Importation des sources
 */
import java.awt.*;

/**
 * Classe Cercle permettant de creer un cercle, cette classe extend de FigureColoree
 * 
 * @author MAYER Th�o, HOF Lucien
 */
public class Cercle extends FigureColoree{

	/**
	 * Attribut r, correspondant au rayon du cercle de type int
	 */
	private int r;
	/**
	 * Attribut fini, permettant de savoir si la construction du cercle est fini de type boolean
	 */
	private boolean fini;
	
	/**
	 * Constructeur de la classe Cercle permettant d initialiser le cercle
	 */
	public Cercle(){
		this.fini=false;
		this.tab_mem=new Point[] {new Point(0,0),new Point(0,0)};
	}
	
	/**
	 * Methode nbPoints permettant de savoir le nombre de points du cercle
	 * 
	 * @return 1
	 */
	@Override
	public int nbPoints() {
		// TODO Auto-generated method stub
		return 1;
	}

	/**
	 * Methode nbClics permettant de savoir le nombre de clics du cercle
	 * 
	 * @return 2
	 */
	@Override
	public int nbClics() {
		// TODO Auto-generated method stub
		return 2;
	}
	
	/**
	 * Methode modifierPoint, permettant de modifier un point du cercle
	 * 
	 * @param p
	 */
	@Override
	public void modifierPoints(Point[] p) {
		this.tab_mem=p;
		this.r=(int)Math.sqrt((this.tab_mem[0].distance(this.tab_mem[1]))*(this.tab_mem[0].distance(this.tab_mem[1]))/2);
	}

	/**
	 * Methode estDedans, permettant de savoir si le pointeur de la souris est dans le cercle
	 * 
	 * @param x
	 * @param y
	 * 
	 * @return res
	 */
	@Override
	public boolean estDedans(int x, int y) {
		boolean res=false;
		Point p = new Point(x,y);
		Point centre = new Point(this.tab_mem[0].rendreX()+this.r/2,this.tab_mem[0].rendreY()+this.r/2);
		System.out.println("r = "+(this.r/2)+", d = "+p.distance(centre));
		if(p.distance(centre)<=this.r/2){
			res=true;
		}
		return res;
	}

	/**
	 * Methode seTranslater, permettant de translater le cercle
	 * 
	 * @param x
	 * @param y
	 */
	@Override
	public void seTranslater(int x, int y) {
		int [] px = new int[tab_mem.length];
		int [] py = new int[tab_mem.length];
		for(int i=0; i<tab_mem.length;i++){
			tab_mem[i].translation(x, y);
			tab_mem[i].translation(x, y);
			px[i] = tab_mem[i].rendreX();
			py[i] = tab_mem[i].rendreY();
			tab_mem[i]=new Point(px[i],py[i]);
		}
		this.r=(int)Math.sqrt((this.tab_mem[0].distance(this.tab_mem[1]))*(this.tab_mem[0].distance(this.tab_mem[1]))/2);
		
	}
	
	/**
	 * Methode affiche, permettant d afficher le cercle
	 * 
	 * @param g
	 */
	public void affiche(Graphics g){
		super.affiche(g);
		if(fini){
			g.setColor(super.getColor());
			g.fillOval(this.tab_mem[0].rendreX(), this.tab_mem[0].rendreY(), this.r, this.r);
		}else{
			g.setColor(Color.BLACK);
			g.drawOval(this.tab_mem[0].rendreX(), this.tab_mem[0].rendreY(), this.r, this.r);
		}
	}
	
	/**
	 * Methode setFini, permettant de set un boolean a fini
	 * 
	 * @param f
	 */
	public void setFini(boolean f){
		this.fini=f;
	}
	
	
}
