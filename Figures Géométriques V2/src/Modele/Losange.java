/**
 * Package correspondant a l'emplacement de la classe
 */
package Modele;

/**
 * Importation des sources
 */
import Modele.Point;

/**
 * Classe Losange permettant de creer un losange, elle extend de Quadrilatere
 * 
 * @author MAYER Th�o, HOF Lucien
 */
public class Losange extends Quadrilatere{

	/**
	 * Constructeur de la classe Losange, permettant d initialiser le losange
	 */
	public Losange(){
		super.fini=false;
	}
	
	/**
	 * Methode nbPoints, permettant de savoir le nombre de point d'un losange
	 * 
	 * @return 4
	 */
	@Override
	public int nbPoints() {
		// TODO Auto-generated method stub
		return 4;
	}
	
	/**
	 * Methode modifierPoints, permettant de modifier les points du losange
	 * 
	 * @param ps
	 */
	public void modifierPoints(Point[] ps) {
		Point[] tab =new Point[4];
		tab[0]=ps[0];
		tab[1]=ps[1];
		tab[2]=new Point(ps[0].rendreX()+((ps[1].rendreX()-ps[0].rendreX())*2),ps[0].rendreY());
		tab[3]=new Point(ps[1].rendreX(),ps[1].rendreY()+(ps[0].rendreY()-ps[1].rendreY())*2);
		super.modifierPoints(tab);
		
	}
	
	/**
	 * Methode setFini, permettant de set un boolean a fini
	 * 
	 * @param f
	 */
	public void setFini(boolean f){
		super.fini=f;
	}

}
