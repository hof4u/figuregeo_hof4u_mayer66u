/**
 * Package correspondant a l'emplacement de la classe
 */
package Modele;

/**
 * Importation des sources
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.Ellipse2D;

/**
 * Classe Ellipse permettant de creer une ellipse, elle extend de FigureColoree
 * 
 * @author MAYER Th�o, HOF Lucien
 */
public class Ellipse extends FigureColoree{
	
	/**
	 * Attribut longueur de type int
	 */
	private int longueur;
	/**
	 * Attribut largeur de type int
	 */
	private int largeur;
	/**
	 * Attribut fini de type boolean
	 */
	private boolean fini;
	
	/**
	 * Methode nbPoints, permettant de savoir le nombre de point d'une ellipse
	 * 
	 * @return 1
	 */
	@Override
	public int nbPoints() {
		// TODO Auto-generated method stub
		return 1;
	}

	/**
	 * Methode nbClics, permettant de savoir le nombre de clic d'une ellipse
	 * 
	 * @return 2
	 */
	@Override
	public int nbClics() {
		// TODO Auto-generated method stub
		return 2;
	}

	/**
	 * Methode modifierPoints, permettant de modifier les points de l ellipse
	 * 
	 * @param p
	 */
	@Override
	public void modifierPoints(Point[] p) {
		this.tab_mem=p;
		this.longueur=this.tab_mem[1].rendreX()-this.tab_mem[0].rendreX();
		this.largeur=this.tab_mem[1].rendreY()-this.tab_mem[0].rendreY();
	}

	/**
	 * Methode estDedans, permettant de savoir si la souris est dans l ellipse
	 * 
	 * @param x
	 * @param y
	 * 
	 * @return eli.contains(x,y)
	 */
	@Override
	public boolean estDedans(int x, int y) {
		Ellipse2D.Double eli = new Ellipse2D.Double(this.tab_mem[0].rendreX(), this.tab_mem[0].rendreY(), this.longueur, this.largeur);
		return eli.contains(x,y);
	}

	/**
	 * Methode seTranslater, permettant de translater l ellipse
	 * 
	 * @param x
	 * @param y
	 */
	@Override
	public void seTranslater(int x, int y) {
		for(int i=0;i<this.tab_mem.length;i++){
			this.tab_mem[i].translation(x, y);
		}
		this.longueur=this.tab_mem[1].rendreX()-this.tab_mem[0].rendreX();
		this.largeur=this.tab_mem[1].rendreY()-this.tab_mem[0].rendreY();
		
	}
	
	/**
	 * Methode setFini, permettant de set un boolean a fini
	 * 
	 * @param f
	 */
	public void setFini(boolean f){
		this.fini=f;
	}
	
	/**
	 * Methode affiche, permettant d afficher l ellipse
	 * 
	 * @param g
	 */
	public void affiche(Graphics g){
		super.affiche(g);
		if(fini){
			g.setColor(super.getColor());
			g.fillOval(this.tab_mem[0].rendreX(), this.tab_mem[0].rendreY(), this.longueur, this.largeur);
		}else{
			g.setColor(Color.BLACK);
			g.drawOval(this.tab_mem[0].rendreX(), this.tab_mem[0].rendreY(), this.longueur, this.largeur);
		}
	}

}
