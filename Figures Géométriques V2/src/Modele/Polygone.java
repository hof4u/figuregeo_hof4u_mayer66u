/**
 * Package correspondant a l'emplacement de la classe
 */
package Modele;

/**
 * Importation des sources
 */
import java.awt.*;

/**
 * Classe abstract Polygone permettant de creer un polygone, elle extend de FigureColoree
 * 
 * @author MAYER Th�o, HOF Lucien
 */
public abstract class Polygone extends FigureColoree{
	
	/**
	 * Attribut p de type Polygon
	 */
	protected Polygon p;	
	/**
	 * Attribut fini de type boolean
	 */
	protected boolean fini;
	
	/**
	 * Constructeur de la classe Polygone, permettant d initialiser un polygone
	 */
	public Polygone(){
		p=new Polygon();
		this.fini=false;
		this.tab_mem=new Point[4];
	}

	/**
	 * Methode nbClics, permettant de savoir le nombre de clics pour un polygone
	 * 
	 * @return 4
	 */
	@Override
	public int nbClics() {
		// TODO Auto-generated method stub
		return 4;
	}
	
	/**
	 * Methode modifierPoints, permettant de modifier les points d un polygone
	 * 
	 * @param pts
	 */
	@Override
	public void modifierPoints(Point[] pts) {
		int [] x = new int[pts.length];
		int [] y = new int[pts.length];
		for(int i=0; i<pts.length;i++){
			this.tab_mem[i]=pts[i];
			x[i] = pts[i].rendreX();
			y[i] = pts[i].rendreY();
			}
		this.p = new Polygon(x,y,pts.length);
	}
	
	/**
	 * Methode seTranslater, permettant de translater le polygone
	 * 
	 * @param x
	 * @param y
	 */
	public void seTranslater(int x, int y){
		int [] px = new int[tab_mem.length];
		int [] py = new int[tab_mem.length];
		for(int i=0; i<tab_mem.length;i++){
			tab_mem[i].translation(x, y);
			px[i] = tab_mem[i].rendreX();
			py[i] = tab_mem[i].rendreY();
			tab_mem[i]=new Point(px[i],py[i]);
		}
		this.p = new Polygon(px,py,tab_mem.length);
	}
	
	/**
	 * Methode affiche, permettant d afficher le polygone
	 * 
	 * @param g
	 */
	public void affiche(Graphics g){
		
		if(fini){
			g.setColor(super.getColor());
			g.fillPolygon(p);
			super.affiche(g);
		}else{
			g.setColor(Color.BLACK);
			g.drawPolygon(p);
		}
	}
	
	/**
	 * Methode estDedans, permettant de savoir si la souris est dans le polygone
	 * 
	 * @param x
	 * @param y
	 * 
	 * @return res
	 */
	public boolean estDedans(int x, int y){
		boolean res=this.p.contains(x,y);
		return res;
	}

	/**
	 * Methode setFini, permettant de set un boolean a fini
	 * 
	 * @param f
	 */
	public void setFini(boolean f){
		this.fini=f;
	}
	
}
