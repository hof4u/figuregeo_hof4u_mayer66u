/**
 * Package correspondant a l'emplacement de la classe
 */
package Modele;

/**
 * Importation des sources
 */
import java.awt.*;

import Modele.Point;

/**
 * Classe Rectangle permettant de creer un rectangle, elle extend de Quadrilatere
 * 
 * @author MAYER Th�o, HOF Lucien
 */
public class Rectangle extends Quadrilatere{

	/**
	 * Constructeur de la classe Rectangle, permettant d initialiser un rectangle
	 */
	public Rectangle(){
		super.fini=false;
	}
	
	/**
	 * Methode nbPoints, permettant de savoir le nombre de points d un rectangle
	 * 
	 * @return 2
	 */
	public int nbPoints(){
		return 2;
	}
	
	/**
	 * Methode modifierPoints, permettant de modifier les points d un rectangle
	 * 
	 * @param p
	 */
	public void modifierPoints(Point[] p) {
		Point[] tab =new Point[4];
		if(p.length>2){
			tab=p;
		}
		else{
			tab[0]=p[0];
			tab[1]=new Point(p[1].rendreX(),p[0].rendreY());
			tab[2]=p[1];
			tab[3]=new Point(p[0].rendreX(),p[1].rendreY());
		}
		super.modifierPoints(tab);
	}
	
	/**
	 * Methode setFini, permettant de set un boolean a fini
	 * 
	 * @param b
	 */
	public void setFini(boolean b){
		this.fini=b;
	}
}
