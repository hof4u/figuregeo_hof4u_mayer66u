/**
 * Package correspondant a l'emplacement de la classe
 */
package Modele;

/**
 * Classe Triangle, permettant de creer un triangle. Cette classe extend de Polygone
 * 
 * @author MAYER Th�o, HOF Lucien
 */
public class Triangle extends Polygone{

	/**
	 * Constructeur de la classe Triangle, permettant d initialiser un triangle
	 */
	public Triangle(){
		this.tab_mem = new Point[] {new Point(0,0),new Point(0,0),new Point(0,0)};
	}
	
	/**
	 * Methode nbPoints, permettant de savoir le nombre de point d'un triangle
	 * 
	 * @return 3
	 */
	@Override
	public int nbPoints() {
		// TODO Auto-generated method stub
		return 3;
	}

}
