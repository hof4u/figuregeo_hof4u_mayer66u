/**
 * Package correspondant a l'emplacement de la classe
 */
package Modele;

/**
 * Importation des sources
 */
import Modele.*;

/**
 * Classe Carre permettant de creer un carre, cette classe extend de Rectangle
 * 
 * @author MAYER Th�o, HOF Lucien
 */
public class Carre extends Rectangle{
	
	/**
	 * Methode nbPoints, permettant d obtenir le nombre de points pour un carre
	 * 
	 * @return 2
	 */
	public int nbPoints(){
		return 2;
	}
	
	/**
	 * Methode modifierPoints, permettant de modifier les points du carre
	 * 
	 * @param p
	 */
	public void modifierPoints(Point[] p) {
		Point[] tab =new Point[4];
		int taille=Math.abs(p[1].rendreX()-p[0].rendreX());
		tab[0]=p[0];
		tab[1]=new Point(p[1].rendreX(),p[0].rendreY());
		tab[2]=new Point(p[0].rendreX()+taille,p[0].rendreY()+taille);
		tab[3]=new Point(p[0].rendreX(),p[0].rendreY()+taille);
		super.modifierPoints(tab);
	}
}
