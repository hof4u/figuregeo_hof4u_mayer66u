/**
 * Package correspondant a l'emplacement de la classe
 */
package Modele;

/**
 * Importation des sources
 */
import java.awt.Graphics;

/**
 * Classe Quadrilatere permettant de creer un quadrilatere, elle extend de Polygone
 * 
 * @author MAYER Th�o, HOF Lucien
 */
public class Quadrilatere extends Polygone{

	/**
	 * Constructeur de la classe Quadrilatere, permettant d initialiser un quadrilatere
	 */
	public Quadrilatere(){
		super.fini=true;
	}
	
	/**
	 * Methode nbPoints, permettant de savoir le nombre de points d un quadrilatere
	 * 
	 * @return 4
	 */
	public int nbPoints(){
		return 4;
	}

	/**
	 * Methode modifierPoints, permettant de modifier les points d un quadrilatere
	 * 
	 * @param p
	 */
	@Override
	public void modifierPoints(Point[] p) {
		super.modifierPoints(p);
		
	}
	
}
