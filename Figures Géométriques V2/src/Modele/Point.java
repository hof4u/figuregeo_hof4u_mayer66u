/**
 * Package correspondant a l'emplacement de la classe
 */
package Modele;

/**
 * Classe Point, qui permet de cr�er un point
 * 
 * @author MAYER Th�o, HOF Lucien
 */
public class Point {

	/**
	 * Attributs x et y de type int
	 */
	private int x,y;
	/**
	 * Attribut select de type boolean
	 */
	private boolean select;
	
	/**
	 * Contructeur de la classe Point, permettant d initialiser un point
	 * @param xx
	 * @param yy
	 */
	public Point(int xx , int yy){
		this.x=xx;
		this.y=yy;
	}
	
	/**
	 * Methode distance, permettant de savoir la distance entre deux points
	 * 
	 * @param p
	 * @return res
	 */
	public double distance(Point p){
		double res=0;
		res=Math.sqrt(((p.x-this.x)*(p.x-this.x))+((p.y-this.y)*(p.y-this.y)));
		return res;
	}
	
	/**
	 * Methode rendreX, permettant de savoir le x du point
	 * 
	 * @return this.x
	 */
	public int rendreX(){
		return this.x;
	}
	
	/**
	 * Methode rendreY, permettant de savoir le y du point
	 * 
	 * @return this.y
	 */
	public int rendreY(){
		return this.y;
	}
	
	/**
	 * Methode incrementerX, permettant d incrementer dx aux coordonnees x
	 * 
	 * @param dx
	 */
	public void incrementerX(int dx){
		this.x=this.x+dx;
	}
	
	/**
	 * Methode incrementerY, permettant d incrementer dy aux coordonnees y
	 * 
	 * @param dy
	 */
	public void incrementerY(int dy){
		this.y=this.y+dy;
	}
	
	/**
	 * Methode modifierX, permettant de modifier le x du point
	 * 
	 * @param mx
	 */
	public void modifierX(int mx){
		this.x=mx;
	}
	
	/**
	 * Methode modifierY, permettant de modifier le y du point
	 * 
	 * @param my
	 */
	public void modifierY(int my){
		this.y=my;
	}
	
	/**
	 * Methode translation, permettant la translation du point
	 * 
	 * @param dx
	 * @param dy
	 */
	public void translation(int dx,int dy){
		this.x=this.x+(dx);
		this.y=this.y+(dy);
	}
	
	/**
	 * Methode estSur, permettant de savoir sur le clique est sur le point
	 * 
	 * @param x
	 * @param y
	 * 
	 * @return res
	 */
	public boolean estSur(int x, int y){
		boolean res=false;
		if(this.distance(new Point(x,y))<=15)
			res=true;
		return res;
	}
	
	/**
	 * Methode selectionner, permettant de selectionner un point
	 */
	public void selectionner(){
		this.select=true;
	}
	
	/**
	 * Methode deSelectionner, permettant de deselectionner un point
	 */
	public void deSelectionner(){
		this.select=false;
	}
	
	/**
	 * Methode isSelect, permettant de savoir si un point est selectionne ou non
	 * 
	 * @return this.select
	 */
	public boolean isSelect(){
		return this.select;
	}
}

