/**
 * Package correspondant a l'emplacement de la classe
 */
package Modele;

/**
 * Importation des sources
 */
import java.awt.*;
import java.util.*;

/**
 * Classe DessinModele permettant de gerer la totalite des modeles, elle extend de Observable
 * 
 * @author MAYER Th�o, HOF Lucien
 */
public class DessinModele extends Observable{

	/**
	 * Attribut nbClic de type int
	 */
	private int nbClic;
	/**
	 * Attribut points_cliques de type ArrayList<Point>
	 */
	private ArrayList<Point> points_cliques;
	/**
	 * Attribut figureEnCours de type FigureColoree
	 */
	private FigureColoree figureEnCours;
	/**
	 * Attribut lfg (liste figure coloree) de type ArrayList<FigureColoree>
	 */
	private ArrayList<FigureColoree> lfg;
	/**
	 * Attribut type de type String
	 */
	private String type;
	/**
	 * Attribut figureZ de type FigureColoree
	 */
	private FigureColoree figureZ;
	/**
	 * Attribut ctrlZ de type String
	 */
	private String ctrlZ;
	
	/**
	 * Constructeur de la classe DessinModele permettant d initialiser un dessin modele
	 */
	public DessinModele(){
		this.lfg=new ArrayList<FigureColoree>();
		this.points_cliques=new ArrayList<Point>();
		this.nbClic=4;
		this.figureEnCours=new Losange();
		this.ctrlZ="efface";
	}
	
	/**
	 * Methode ajoute, permettant d ajouter une figure
	 * 
	 * @param fc
	 */
	public void ajoute(FigureColoree fc){
		lfg.add(fc);
		setChanged();

		notifyObservers();
		this.figureEnCours=null;
		this.points_cliques.clear();
		
	}
	
	/**
	 * Methode suprLast, permettant de supprimer la derniere figure coloree
	 */
	public void suprLast(){
		if(lfg.isEmpty()==false)
			lfg.remove(lfg.size()-1);
	}
	
	/**
	 * Methode changerCoul, permettant de changer la couleur d une figure
	 * 
	 * @param fc
	 * @param c
	 */
	public void changerCoul(FigureColoree fc,Color c){
		fc.changerCouleur(c);
	}
	
	/**
	 * Methode construit, permettant de construire une figure
	 * 
	 * @param fc
	 * @param c
	 */
	public void construit(FigureColoree fc,Color c){
		this.figureEnCours=fc;
		this.figureEnCours.changerCouleur(c);
		if(this.figureEnCours instanceof Trace==false)
			this.figureEnCours.selectionne();
		setChanged();

		notifyObservers();
	}
	
	/**
	 * Methode ajoutPt, permettant d ajouter un point sur le dessin
	 * 
	 * @param x
	 * @param y
	 */
	public void ajoutePt(int x, int y){
		this.points_cliques.add(new Point(x,y));
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Methode getNbClics, permettant d obtenir le nombre de clics
	 * 
	 * @return this.nbClic
	 */
	public int getNbClic(){
		return this.nbClic;
	}
	
	/**
	 * Methode setNbClic, permettant de set un nombre a NbClic
	 * 
	 * @param nb
	 */
	public void setNbClic(int nb){
		this.nbClic=nb;
	}
	
	/**
	 * Methode getLfg, permettant d obtenir l ArrayList de figure coloree
	 * 
	 * @return this.lfg
	 */
	public ArrayList<FigureColoree> getLfg(){
		setChanged();
		
		notifyObservers();
		return this.lfg;
	}
	
	/**
	 * Methode setLfg, permettant de set une ArrayList a lfg
	 * 
	 * @param lfc
	 */
	public void setLfg(ArrayList<FigureColoree> lfc){
		this.lfg=lfc;
	}
	
	/**
	 * Methode getFigureEnCours, permettant d obtenir la figure en cours
	 * 
	 * @return this.figureEnCours
	 */
	public FigureColoree getFigureEnCours(){
		setChanged();

		notifyObservers();
		return this.figureEnCours;
	}
	
	/**
	 * Methode setFigureEnCours, permettant de set une figure en cours a l attribut figureencours
	 * 
	 * @param fc
	 */
	public void setFigureEnCours(FigureColoree fc){
		this.figureEnCours=fc;
	}
	
	/**
	 * Methode getClics, permettant d obtenir les clics qu on a effectue
	 * 
	 * @return this.points_cliques
	 */
	public ArrayList<Point> getClics(){
		return this.points_cliques;
	}
	
	/**
	 * Methode estDedans, permettant de savoir si le clic est dans la figure
	 * 
	 * @param x
	 * @param y
	 * @param i
	 * @return res
	 */
	public boolean estDedans(int x, int y, int i){
		boolean res=false;
		res=this.lfg.get(i).estDedans(x, y);
		return res;
	}
	
	/**
	 * Methode setFigureZ, permettant de set une figure coloree a figureZ
	 * 
	 * @param fc
	 */
	public void setFigureZ(FigureColoree fc){
		this.figureZ=fc;
	}
	
	/**
	 * Methode getFigureZ, permettant d obtenir la figureZ
	 * 
	 * @return this.figureZ
	 */
	public FigureColoree getFigureZ(){
		return this.figureZ;
	}
	
	/**
	 * Methode setCtrlZ, permettant de set un string z a l attribut ctrlZ
	 * 
	 * @param z
	 */
	public void setCtrlZ(String z){
		this.ctrlZ=z;
	}
	
	/**
	 * Methode getCtrlZ, permettant d obtenir la chaine de caractere de ctrlZ
	 * 
	 * @return this.ctrlZ
	 */
	public String getCtrlZ(){
		return this.ctrlZ;
	}
	
	/**
	 * Methode setType, permettant de set un tring t a l attribut type
	 * 
	 * @param t
	 */
	public void setType(String t){
		this.type=t;
	}
	
	/**
	 * Methode getType, permettant d obtenir le type
	 * 
	 * @return this.type
	 */
	public String getType(){
		return this.type;
	}
	
	
	
	
}
