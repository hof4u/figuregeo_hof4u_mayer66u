/**
 * Package correspondant a l'emplacement de la classe
 */
package Vue;

/**
 * Importation des sources
 */
import java.awt.*;
import java.util.*;

import javax.swing.*;

import Modele.*;

/**
 * Classe VueDessin permettant de visualiser le panel de dessin. Cette classe extend de JPanem et implement d Observer
 * 
 * @author MAYER Th�o, HOF Lucien
 */
public class VueDessin extends JPanel implements Observer{
	
	/**
	 * Attribut salade de type DessinModele
	 */
	private DessinModele salade;
	
	/**
	 * Constructeur de la classe VueDessin, permettant d initialiser une VueDessin
	 */
	public VueDessin(){
		salade = new DessinModele();
		this.setPreferredSize(new Dimension(500,500));
		this.setBackground(Color.WHITE);
	}

	/**
	 * Methode update, permettant de faire une update sur le modele
	 * 
	 * @param o, Observable
	 * @param arg, Object
	 */
	@Override
	public void update(Observable o, Object arg) {
		salade = (DessinModele)o;
		repaint();
		
	}
	
	/**
	 * Methode paintComponent, permettant de dessiner les choses dont on a besoin
	 * 
	 * @param g
	 */
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		for(int i=0;i<salade.getClics().size();i++){
			g.setColor(Color.BLACK);
			g.drawLine(this.salade.getClics().get(i).rendreX()-5, this.salade.getClics().get(i).rendreY(), this.salade.getClics().get(i).rendreX()+5, this.salade.getClics().get(i).rendreY());
			g.drawLine(this.salade.getClics().get(i).rendreX(), this.salade.getClics().get(i).rendreY()-5, this.salade.getClics().get(i).rendreX(), this.salade.getClics().get(i).rendreY()+5);
		}
		for(int i=0;i<salade.getLfg().size();i++){
			if(salade.getLfg().get(i)!=null)
				salade.getLfg().get(i).affiche(g);
			
		}
		if(salade.getFigureEnCours()!=null)
			salade.getFigureEnCours().affiche(g);
		
		
	}

}
