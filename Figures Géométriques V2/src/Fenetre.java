/**
 * Importation des sources
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileNameExtensionFilter;

import Controleur.*;
import Modele.*;
import Vue.*;

/**
 * Classe Fenetre, permettant de creer la fenetre. Cette classe extend de JFrame
 * 
 * @author MAYER Theo, HOF Lucien
 */
public class Fenetre extends JFrame{
	
	/**
	 * Attribut dessin de type VueDessin
	 */
	VueDessin dessin;
	/**
	 * Attribut model de type DessinModele
	 */
	DessinModele model;
	
	/**
	 * Constructeur de la classe Fenetre, permettant d initialiser le fenetre
	 * 
	 * @param s
	 * @param l
	 * @param h
	 * @throws IOException
	 */
	public Fenetre(String s, int l, int h) throws IOException{
		
		
		this.dessin = new VueDessin();
		this.dessin.setPreferredSize(new Dimension(l,70));
		
		this.model = new DessinModele();
		this.model.addObserver(dessin);
		
		PanneauChoix boutons = new PanneauChoix(model);
		
		
		this.setTitle(s);
		this.setPreferredSize(new Dimension(l,h));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		
		this.add(boutons,BorderLayout.NORTH);
		
		this.add(this.dessin, BorderLayout.CENTER);
	
		
		
		
		this.pack();
		this.requestFocusInWindow();
		this.setVisible(true);
		
		FabricantFigures ff = new FabricantFigures(model);
		ManipulateurFormes mf = new ManipulateurFormes(model);
		FabricantTrace ft = new FabricantTrace(model);
		RacClav rc = new RacClav(model);
		dessin.addMouseListener(ff);
		dessin.addMouseMotionListener(ff);
		dessin.addMouseListener(mf);
		dessin.addMouseMotionListener(mf);
		dessin.addMouseListener(ft);
		dessin.addMouseMotionListener(ft);
		dessin.addKeyListener(rc);
		dessin.requestFocusInWindow();
		
		//Initialisation Menu
        JMenu menu = new JMenu("Menu");
        menu.setMnemonic(KeyEvent.VK_A);
        JMenuBar menuBar= new JMenuBar();
		menuBar.add(menu);
		
		//Bouton ouvrir
		JMenuItem menuItemOuv = new JMenuItem("Ouvrir", KeyEvent.VK_T);
		menuItemOuv.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		//Listener sur ce enregistrer
		menuItemOuv.addActionListener(new ActionListener() {
			@Override	
			public void actionPerformed(ActionEvent e) {
				try{
					JFileChooser chooser = new JFileChooser();  
					//Petit filtre mvc
					chooser.setFileFilter(new FileNameExtensionFilter("Fichier Dessin", "mvc"));
					//Dossier Courant
					chooser.setCurrentDirectory(new  File("."+File.separator)); 
					//Affichage et r�cup�ration de la r�ponse de l'utilisateur
					int reponse = chooser.showDialog(chooser,"Ouvrir");
					// Si l'utilisateur clique sur OK
					if  (reponse == JFileChooser.APPROVE_OPTION){                
					// R�cup�ration du chemin du fichier
					String  fichier= chooser.getSelectedFile().toString();           
					importer(fichier);
					}
				}catch(HeadlessException he){
					he.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
						e1.printStackTrace();
				}
			}

			private void importer(String fichier) throws ClassNotFoundException {
				System.out.println("Chargement...");
				try{
					ObjectInputStream w = new ObjectInputStream(new FileInputStream(fichier));
					w.close();
					dessin.repaint();
				}catch (IOException e){
					e.printStackTrace();
				}
			}				
		});
		
		menu.add(menuItemOuv);
	
		//Bouton nouveau
		JMenuItem menuItemNew = new JMenuItem("Nouveau (en concervant cette fen�tre)", KeyEvent.VK_T);
		menuItemNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, ActionEvent.ALT_MASK));
		//Listener sur nouveau
		menuItemNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				try {
					reinitialiser(0);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});		
		
		menu.add(menuItemNew);
				
		//Bouton nouveau (action l�g�rement diff�rente)
		JMenuItem menuItemNew2 = new JMenuItem("Nouveau (sans conserver cette fen�tre)", KeyEvent.VK_T);
		menuItemNew2.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_3, ActionEvent.ALT_MASK));
		//Listener sur nouveau
		menuItemNew2.addActionListener(new ActionListener() {
			@Override	   
			public void actionPerformed(ActionEvent e) {
				try {
					reinitialiser(1);
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		});
		
		menu.add(menuItemNew2);
				
		//Ajout d'un separateur par soucis de lisibilit�
		menu.add(new JSeparator());
				
		JMenuItem menuItemEnr = new JMenuItem("Enregistrer Projet", KeyEvent.VK_T);
		menuItemEnr.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		//Listener sur enregistrer
		menuItemEnr.addActionListener(new ActionListener() {
			@Override	
			public void actionPerformed(ActionEvent e) {
				try{
					JFileChooser chooser = new JFileChooser();      
					//Petit filtre mvc
					chooser.setFileFilter(new FileNameExtensionFilter("Fichier Dessin", "mvc"));
					// Dossier Courant
					chooser.setCurrentDirectory(new  File("."+File.separator)); 
					//Affichage et r�cup�ration de la r�ponse de l'utilisateur
					int reponse = chooser.showDialog(chooser,"Enregistrer Projet");
					// Si l'utilisateur clique sur OK
					if (reponse == JFileChooser.APPROVE_OPTION){                
						// R�cup�ration du chemin du fichier
						String  fichier= chooser.getSelectedFile().toString();           
						enregister(fichier);
						JOptionPane.showMessageDialog(Fenetre.this, "Votre image a bien �t� enregistr�e au format mvc", "Enregistr� avec succes", JOptionPane.INFORMATION_MESSAGE);
					}
				}catch(HeadlessException he){
					he.printStackTrace();
					JOptionPane.showMessageDialog(Fenetre.this, "Une erreur est survenue lors de l'enregistrement", "Erreur", JOptionPane.ERROR_MESSAGE); 
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					JOptionPane.showMessageDialog(Fenetre.this, "Une erreur est survenue lors de l'enregistrement", "Erreur", JOptionPane.ERROR_MESSAGE); 
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					JOptionPane.showMessageDialog(Fenetre.this, "Une erreur est survenue lors de l'enregistrement", "Erreur", JOptionPane.ERROR_MESSAGE); 
				}
			}

			private void enregister(String fichier) throws FileNotFoundException, IOException {
				System.out.println("Sauvegarde...\n");
				if(!(fichier.contains(".mvc")))fichier =  fichier+".mvc";
				ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(fichier));
			}
							
		});
		menu.add(menuItemEnr);
				
		//Bouton enregistrer
		JMenuItem menuItem = new JMenuItem("Enregistrer image dessin", KeyEvent.VK_T);
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, ActionEvent.CTRL_MASK));
		//Listener sur ce enregistrer
		menuItem.addActionListener(new ActionListener() {
			@Override	
			public void actionPerformed(ActionEvent e) {
				try{
					JFileChooser chooser = new JFileChooser(); 
					// Dossier Courant
					chooser.setCurrentDirectory(new  File("."+File.separator));
					//Affichage et r�cup�ration de la r�ponse de l'utilisateur
					int reponse = chooser.showDialog(chooser,"Enregistrer image");
					// Si l'utilisateur clique sur OK
					if (reponse == JFileChooser.APPROVE_OPTION){                
						// R�cup�ration du chemin du fichier
						String  fichier=chooser.getSelectedFile().toString();           
						capture(fichier);
					}
				}catch(HeadlessException he){
					he.printStackTrace();
				}
			}
							
		});
		menu.add(menuItem);
				
				
				
		//Ajout d'un deuxi�me separateur pour la meme raison que le pr�c�dent
		menu.add(new JSeparator());
				
		//Bouton exit permettant de quitter toutes les fen�tres
		JMenuItem menuItem1 = new JMenuItem("Fermer",KeyEvent.VK_T);
		menuItem1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_8, ActionEvent.ALT_MASK));
					
		menuItem1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				System.exit(0);
			}
		});		
						
		menu.add(menuItem1);
				
		//On finalise les attributs et les param�tres de la fen�tre (en ajoutant notament ce qui vient d'�tre cr��)
		this.setJMenuBar(menuBar);
		menuBar.setVisible(true);
		this.pack();
		this.setResizable(false);
		this.setFocusable(true);
		this.requestFocusInWindow();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
			
			
	/**
	 * Methode reinitialiser, permettant de fermer la fenetre en cours (this.dispose) et de relancer une nouvelle (gr�ce au main)
	 * 
	 * @param a
	 * @throws IOException
	 */
	public void reinitialiser(int a) throws IOException{
		if(a==1){
			this.dispose();
		}
		main(null);
	}
			
	/**
	 * Methode capture, permettant d enregistre le dessin sous format choisi (jpg, jpeg et png(par defaut))
	 * 
	 * @param s
	 */
	public void capture(String s){
		BufferedImage bI = new BufferedImage(this.dessin.getWidth(),this.dessin.getHeight(),BufferedImage.TYPE_BYTE_INDEXED); 
		this.dessin.paint(bI.getGraphics()); 
		String format;
		try { 
			if(s.contains(".jpeg")){
				File f = new File(s);
				FileOutputStream fichier = new FileOutputStream(f); 
				ImageIO.write(bI, "jpeg", fichier); 
				fichier.close();
				format = "jpeg";
			}else if(s.contains(".jpg")){
				File f = new File(s);
				FileOutputStream fichier = new FileOutputStream(f); 
				ImageIO.write(bI, "jpg", fichier); 
				fichier.close();
				format = "jpg";
			}else if(s.contains(".png")){
				File f = new File(s);
				FileOutputStream fichier = new FileOutputStream(f); 
				ImageIO.write(bI, "png", fichier); 
				fichier.close();
				format = "png";
			}else {
				File f = new File(s+".png");
				FileOutputStream fichier = new FileOutputStream(f); 
				ImageIO.write(bI, "png", fichier); 
				fichier.close();
				format = "png";
			}			
			JOptionPane.showMessageDialog(this, "Votre image a bien �t� enregistr�e au format "+format, "Enregistr� avec succes", JOptionPane.INFORMATION_MESSAGE);

		} catch (Exception e) { 
			} 
	} 
		
	/**
	 * Methode main, permettant de creer une fenetre
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {	
		Fenetre f = new Fenetre("Formes g�ometriques", 1400,900);		
	}

}
